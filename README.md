# tdg
TODO get - tool to extract todo tasks from source code

[![Go Report Card](https://goreportcard.com/badge/github.com/ribtoks/tdg)](https://goreportcard.com/report/gitlab.com/ribtoks/tdg)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/712b5193d6564beb88ba1e66ac1e0792)](https://www.codacy.com/app/ribtoks/tdg)
[![Maintainability](https://api.codeclimate.com/v1/badges/89dad5db195c7b5d3e90/maintainability)](https://codeclimate.com/gitlab/ribtoks/tdg/maintainability)

![license](https://img.shields.io/badge/license-Apache_2.0-blue.svg)
![copyright](https://img.shields.io/badge/%C2%A9-Taras_Kushnir-blue.svg)
![language](https://img.shields.io/badge/language-go-blue.svg)

## About

This tool generates json from comments contained in the source code. Main use-case for it is to create automatic issues based on TODO/FIXME/BUG/HACK comments. This tool supports additional tag information in the comment (Category, Issue etc.).

There are two useful applications in project management automation: [converting TODOs into GitHub Issues](https://github.com/ribtoks/tdg-github-action) and synchronization with Kanboard using [tdg-import](https://github.com/ribtoks/kanboard-tdg-import) plugin.

Example of the comment (everything but the first line is optional):

    // TODO: This is title of the issue to create
    // category=SomeCategory issue=123 estimate=30m author=alias
    // This is a multiline description of the issue
    // that will be in the "Body" property of the comment

Sample generated json (if run in this repository root as `tdg -verbose -blame`):

```json
    {
      "root": "/Users/ribtoks/Projects/go/src/github.com/ribtoks/tdg",
      "branch": "master",
      "author": "Taras Kushnir",
      "project": "tdg",
      "comments": [
        {
          "type": "TODO",
          "title": "This is title of the issue to create",
          "body": "This is a multiline description of the issue\nthat will be in the \"Body\" property of the comment",
          "file": "README.md",
          "line": 20,
          "issue": 123,
          "author": "alias",
          "commitHash": "8a9e18b1e0fecf5e94203db058ca5110d05aaf29",
          "committerEmail": "foo@bar.com",
          "category": "SomeCategory",
          "estimate": 0.5
        }
      ]
    }
```

Supported comments: `//`, `/*`, `#`, `%`, `;;` (adding new supported comments is trivial).

## Install

As simple as

    go install github.com/ribtoks/tdg/cmd/tdg@latest

## Build

As simple as

    make build

## Usage

    -exclude value
      	Exclude pattern (can be specified multiple times)
    -help
      	Show help
    -include value
      	Include pattern (can be specified multiple times)
    -log string
      	Path to the logfile (default "tdg.log")
    -min-chars int
      	Include comments with more chars than this (default 30)
    -min-words int
      	Skip comments with less than minimum words (default 3)
    -root string
      	Path to the the root of source code (default "./")
    -blame
        Get commit hash and commiter email of the comments
    -stdout
      	Duplicate logs to stdout
    -verbose
      	Output human-readable json

## Examples:

### With include pattern and verbose flag

    tdg -root ~/Projects/xpiks-root/xpiks/src/ -include "\.(cpp|h)$" -verbose

Include pattern is a regexp. With verbose flag you get human-readable json and log output in stdout. Without verbose flag this tool could be used as input for smth else like `curl`.

### With blame flag

    tdg -blame -verbose

Includes the commit hash and committer email in comment json. Adds these fields per comment:

```diff
14a15,16
>           "commitHash": "8a9e18b1e0fecf5e94203db058ca5110d05aaf29",
>           "committerEmail": "foo@bar.com",
```

## How to contribute

-   [Fork](http://help.github.com/forking/) tdg repository on GitHub
-   Clone your fork locally
-   Configure the upstream repo (`git remote add upstream git@github.com:ribtoks/tdg.git`)
-   Create local branch (`git checkout -b your_feature`)
-   Work on your feature
-   Build and Run tests (`go tests -v`)
-   Push the branch to GitHub (`git push origin your_feature`)
-   Send a [pull request](https://help.github.com/articles/using-pull-requests) on GitHub
