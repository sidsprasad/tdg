build:
	export GOFLAGS="-mod=vendor"
	go build -o bin/tdg cmd/tdg/*

vendors:
	go mod tidy
	go mod vendor
